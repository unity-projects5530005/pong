using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balle : MonoBehaviour
{
    [SerializeField]
    private int speedBall;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        // V�rifie si la collision a lieu avec une barre de joueur
        if(collision.gameObject.tag == "players")
        {
            this.GetComponent<Rigidbody>().velocity = new Vector3(-this.GetComponent<Rigidbody>().velocity.x * speedBall,
                0, -this.GetComponent<Rigidbody>().velocity.z) * speedBall;
        }
    }
}
