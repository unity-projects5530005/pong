using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;

public class Game : MonoBehaviour
{
    [SerializeField]
    private GameObject player1;
    [SerializeField]
    private int pointsPlayer1; 
    [SerializeField]
    private float movePlayer1;

    [SerializeField]
    private GameObject player2;
    [SerializeField]
    private int pointsPlayer2;
    [SerializeField]
    private float movePlayer2;

    [SerializeField]
    private GameObject ball;
    [SerializeField]
    private int speedBall;

    [SerializeField]
    private TMP_Text textScore;

    // Start is called before the first frame update
    void Start()
    {
        float x = Random.Range(speedBall * -1, speedBall);
        float z = Random.Range(speedBall * -1, speedBall);

        ball.GetComponent<Rigidbody>().velocity = new Vector3(x * speedBall, 0, z * speedBall);
    }

    // Update is called once per frame
    void Update()
    {
        // Appelle la m�thode pour bouger les joueurs
        movePlayer();
    }

    // Met � jour le compteur de points
    public void updateScore()
    {
        textScore.text = "Score : " + pointsPlayer1.ToString() + " - " + pointsPlayer2.ToString();
    }

    private void movePlayer()
    {
        // Bouge le joueur 1 (avec les touches Z/S)
        if (Input.GetKeyDown(KeyCode.Z))
        {
            player1.GetComponent<Rigidbody>().velocity = player1.transform.forward * movePlayer1;
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            player1.GetComponent<Rigidbody>().velocity = player1.transform.forward * -movePlayer1;
        }

        // Bouge le joueur 2 (avec les touches O/L)
        if (Input.GetKeyDown(KeyCode.O))
        {
            player2.GetComponent<Rigidbody>().velocity = player2.transform.forward * movePlayer2;
        }
        else if (Input.GetKeyDown(KeyCode.L))
        {
            player2.GetComponent<Rigidbody>().velocity = player2.transform.forward * -movePlayer2;
        }
    }
}
